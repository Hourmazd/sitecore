﻿namespace Infrastructure.Notification
{
    public class NotificationDTO<T>
        where T : class
    {
        public T Entity { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
