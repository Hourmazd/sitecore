﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Domain.Entities;
using System.Collections.Generic;
using Application.AssetCQRS.Commands;
using Application.AssetCQRS.Queries;
using System;

namespace Web.API.Controllers
{
    //[Authorize]
    public class AssetController : BaseController
    {
        /// <summary>
        /// Get the whole data
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet("{pageSize}/{page}")]
        public async Task<ActionResult<IEnumerable<Asset>>> GetAll(int pageSize, int page)
        {
            return Ok(await Mediator.Send(new AssetFetchListCommand { PageSize = pageSize, Page = page }));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Asset>> Get(Guid id)
        {
            return Ok(await Mediator.Send(new AssetFetchCommand() { AssetId = id }));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Upsert(UpsertAssetCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await Mediator.Send(new DeleteAssetCommand { AssetId = id });

            return NoContent();
        }
    }
}
