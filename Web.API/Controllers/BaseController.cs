﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;


namespace Web.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public abstract class BaseController : ControllerBase
    {
        private IMediator _Mediator;

        protected IMediator Mediator => _Mediator ??= HttpContext.RequestServices.GetService<IMediator>();
    }
}
