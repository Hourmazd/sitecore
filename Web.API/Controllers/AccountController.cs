﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Web.API.Controllers
{
    [AllowAnonymous]
    public class AccountController : BaseController
    {
        private readonly UserManager<IdentityUser> _UserManager;
        private readonly SignInManager<IdentityUser> _SignInManager;

        public AccountController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            _UserManager = userManager;
            _SignInManager = signInManager;
        }
        
        [HttpPost("register/{email}/{password}")]
        public async Task<IdentityResult> RegisterUser(string email, string password)
        {
            var user = new IdentityUser { UserName = email, Email = email };
            return await _UserManager.CreateAsync(user, password);
        }

        [HttpPost("signin")]
        public async Task SignIn(IdentityUser user)
        {
            await _SignInManager.SignInAsync(user, isPersistent: false);
        }

        [HttpPost("signin/{email}/{password}/{remember}")]
        public async Task<bool> SignIn(string email, string password, bool remember)
        {
            var result = await _SignInManager.PasswordSignInAsync(email, password, remember, false);
            return result.Succeeded;
        }
    }
}