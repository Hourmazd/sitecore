﻿using System.IO;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Domain.Entities;

namespace Application.Helpers
{
    public class CSVImportHelper
    {
        private static CSVImportHelper _Instance;

        public static CSVImportHelper Instance => _Instance ??= new CSVImportHelper();

        #region Public Methods

        public IEnumerable<Asset> GetAssets()
        {
            var path = Directory.GetCurrentDirectory() + "\\AssetImport.csv";

            using var reader = new StreamReader(path, Encoding.UTF8);
            using var csvReader = new CsvHelper.CsvReader(reader);
            return csvReader.GetRecords<Asset>().ToArray();
        }

        #endregion
    }
}
