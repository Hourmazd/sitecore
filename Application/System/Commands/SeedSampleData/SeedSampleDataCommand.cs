﻿using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Application.Interfaces;

namespace Application.System.Commands.SeedSampleData
{
    public class SeedSampleDataCommand : IRequest
    {
    }

    public class SeedSampleDataCommandHandler : IRequestHandler<SeedSampleDataCommand>
    {
        private readonly IAssetDbContext _Context;

        public SeedSampleDataCommandHandler(IAssetDbContext context)
        {
            _Context = context;
        }

        public async Task<Unit> Handle(SeedSampleDataCommand request, CancellationToken cancellationToken)
        {
            var assets = Helpers.CSVImportHelper.Instance.GetAssets();

            _Context.Assets.AddRange(assets);

            await _Context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
