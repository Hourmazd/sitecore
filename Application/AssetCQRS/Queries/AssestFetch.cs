﻿using System;
using MediatR;
using Application.Interfaces;
using System.Threading;
using System.Threading.Tasks;
using Domain.Entities;

namespace Application.AssetCQRS.Queries
{
    public class AssetFetchCommand : IRequest<Asset>
    {
        public Guid AssetId { get; set; }
    }

    public class AssetFetchHandler : IRequestHandler<AssetFetchCommand, Asset>
    {
        private readonly IAssetDbContext _Context;

        public AssetFetchHandler(IAssetDbContext context)
        {
            _Context = context;
        }

        public async Task<Asset> Handle(AssetFetchCommand request, CancellationToken cancellationToken)
        {
            return await _Context.Assets.FindAsync(request.AssetId);
        }
    }
}
