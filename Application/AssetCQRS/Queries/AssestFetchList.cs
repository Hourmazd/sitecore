﻿using System.Linq;
using System.Collections.Generic;
using MediatR;
using Application.Interfaces;
using System.Threading;
using System.Threading.Tasks;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Application.AssetCQRS.Queries
{
    public class AssetFetchListCommand : IRequest<IEnumerable<Asset>>
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
    }

    public class AssetFetchListHandler : IRequestHandler<AssetFetchListCommand, IEnumerable<Asset>>
    {
        private readonly IAssetDbContext _Context;

        public AssetFetchListHandler(IAssetDbContext context)
        {
            _Context = context;
        }

        public async Task<IEnumerable<Asset>> Handle(AssetFetchListCommand request, CancellationToken cancellationToken)
        {
            return await _Context.Assets
                .Skip((request.Page - 1) * request.PageSize)
                .Take(request.PageSize)
                .ToListAsync(cancellationToken);
        }
    }
}
