﻿using System;
using MediatR;
using Application.Interfaces;
using System.Threading;
using System.Threading.Tasks;
using Domain.Entities;

namespace Application.AssetCQRS.Commands
{
    public class UpsertAssetCommand : IRequest<Asset>
    {
        public Guid? AssetId { get; set; }

        public string FileName { get; set; }

        public string MimeType { get; set; }

        public string CreatedBy { get; set; }

        public string Email { get; set; }

        public string Country { get; set; }

        public string Description { get; set; }
    }

    public class UpsertAssetCommandHandler : IRequestHandler<UpsertAssetCommand, Asset>
    {
        private readonly IAssetDbContext _Context;
        private readonly IMediator _Mediator;

        public UpsertAssetCommandHandler(IAssetDbContext context, IMediator mediator)
        {
            _Context = context;
            _Mediator = mediator;
        }

        public async Task<Asset> Handle(UpsertAssetCommand request, CancellationToken cancellationToken)
        {
            Asset entity;

            if (request.AssetId.HasValue)
            {
                entity = await _Context.Assets.FindAsync(request.AssetId.Value);
            }
            else
            {
                entity = new Asset()
                {
                    AssetId = Guid.NewGuid()
                };

                _Context.Assets.Add(entity);
            }

            entity.Country = request.Country;
            entity.CreatedBy = request.CreatedBy;
            entity.Description = request.Description;
            entity.Email = request.Email;
            entity.FileName = request.FileName;
            entity.MimeType = request.MimeType;

            var success = await _Context.SaveChangesAsync(cancellationToken) > 0;

            if (!success)
                throw new Exception("Problem saving changes");

            await _Mediator.Publish(new AssetNotification() { Entity = entity }, cancellationToken);

            return entity;
        }
    }
}
