﻿using System;
using MediatR;
using Application.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace Application.AssetCQRS.Commands
{
    public class DeleteAssetCommand : IRequest
    {
        public Guid AssetId { get; set; }
    }

    public class DeleteAssetCommandHandler : IRequestHandler<DeleteAssetCommand>
    {
        private readonly IAssetDbContext _Context;

        public DeleteAssetCommandHandler(IAssetDbContext context)
        {
            _Context = context;
        }

        public async Task<Unit> Handle(DeleteAssetCommand request, CancellationToken cancellationToken)
        {
            var entity = await _Context.Assets.FindAsync(request.AssetId);

            if (entity == null)
            {
                throw new Exception($"Unable to find asset id: {request.AssetId}");
            }

            _Context.Assets.Remove(entity);

            await _Context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
