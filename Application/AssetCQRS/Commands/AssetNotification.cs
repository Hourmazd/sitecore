﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Domain.Entities;
using Infrastructure.Notification;
using MediatR;

namespace Application.AssetCQRS.Commands
{
    public class AssetNotification : INotification
    {
        public Asset Entity;
    }

    public class AssetNotificationHandler : INotificationHandler<AssetNotification>
    {
        private readonly NotificationService _NotificationService;

        public AssetNotificationHandler(NotificationService notificationService)
        {
            _NotificationService = notificationService;
        }

        public async Task Handle(AssetNotification notification, CancellationToken cancellationToken)
        {
            await _NotificationService.SendNotificationAsync(new NotificationDTO<Asset>() { Entity = notification.Entity });
        }
    }
}
