﻿using Microsoft.EntityFrameworkCore;
using Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IAssetDbContext
    {
        DbSet<Asset> Assets { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
