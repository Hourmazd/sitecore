﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.AssetCQRS.Commands;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Sitecore.Test.Common;

namespace Sitecore.Test.DataManipulation
{
    [TestClass]
    public class AssetDataManipulationTest : CommonTestBase
    {
        public static IEnumerable<object[]> GetSampleData()
        {
            yield return new object[] { "filename01", "mimetype01" };
            yield return new object[] { "filename02", "mimetype02" };
            yield return new object[] { "filename03", "mimetype03" };
        }

        [DataTestMethod]
        [DynamicData(nameof(GetSampleData), DynamicDataSourceType.Method)]
        public async Task AddAsset(string fileName, string mimeType)
        {
            var command = new UpsertAssetCommand()
            {
                FileName = fileName,
                MimeType = mimeType
            };

            var handler = new UpsertAssetCommandHandler(_Context, _Mediator.Object);
            var entity = await handler.Handle(command, CancellationToken.None);

            //Test result
            Assert.AreEqual(entity.FileName, fileName);

            //Test Mediator
            _Mediator.Verify(e => e.Publish(It.Is<AssetNotification>(s => s.Entity.FileName == fileName), It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public async Task DeleteAsset()
        {
            var list = await SeedSampleData(1);
            Exception expectedEx = null;

            //Part 1
            var command = new DeleteAssetCommand()
            {
                AssetId = list.First().AssetId
            };

            try
            {
                var handler = new DeleteAssetCommandHandler(_Context);
                await handler.Handle(command, CancellationToken.None);
            }
            catch (Exception e)
            {
                expectedEx = e;
            }

            Assert.IsNull(expectedEx);

            //Part 2
            command = new DeleteAssetCommand()
            {
                AssetId = Guid.NewGuid()
            };

            try
            {
                var handler = new DeleteAssetCommandHandler(_Context);
                await handler.Handle(command, CancellationToken.None);
            }
            catch (Exception e)
            {
                expectedEx = e;
            }

            Assert.IsNotNull(expectedEx);
        }
    }
}
