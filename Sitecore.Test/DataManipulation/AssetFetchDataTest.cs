﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.AssetCQRS.Queries;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sitecore.Test.Common;

namespace Sitecore.Test.DataManipulation
{
    [TestClass]
    public class AssetFetchDataTest : CommonTestBase
    {
        [TestMethod]
        public async Task ReadAsset()
        {
            // Seed Data
            var list = await SeedSampleData(2);

            var command = new AssetFetchCommand()
            {
                AssetId = list.First().AssetId
            };

            var handler = new AssetFetchHandler(_Context);
            var result = await handler.Handle(command, CancellationToken.None);

            Assert.AreEqual(result.AssetId, list.First().AssetId);
        }

        [TestMethod]
        public async Task ReadAssetList()
        {
            // Seed Data
            await SeedSampleData(20);

            // Part 1 =======================================
            var command = new AssetFetchListCommand()
            {
                Page = 1,
                PageSize = 10
            };

            var handler = new AssetFetchListHandler(_Context);
            var result = await handler.Handle(command, CancellationToken.None);

            Assert.AreEqual(result.Count(), 10);

            // Part 2 =======================================
            command = new AssetFetchListCommand()
            {
                Page = 1,
                PageSize = 100
            };

            handler = new AssetFetchListHandler(_Context);
            result = await handler.Handle(command, CancellationToken.None);

            Assert.AreEqual(result.Count(), 20);
        }
    }
}
