﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;
using MediatR;
using Moq;
using Persistence;

namespace Sitecore.Test.Common
{
    public class CommonTestBase : IDisposable
    {
        protected readonly AssetDbContext _Context;
        protected readonly Mock<IMediator> _Mediator;

        public CommonTestBase()
        {
            _Context = AssetDbContextFactory.Create();
            _Mediator = new Mock<IMediator>();
        }

        public void Dispose()
        {
            AssetDbContextFactory.Destroy(_Context);
        }
        protected async Task<IList<Asset>> SeedSampleData(int rowCount)
        {
            var list = new List<Asset>();

            for (var i = 0; i < rowCount; i++)
                list.Add(new Asset { AssetId = Guid.NewGuid(), FileName = "filename" + i });

            await _Context.Assets.AddRangeAsync(list);
            await _Context.SaveChangesAsync();

            return list;
        }
    }
}
