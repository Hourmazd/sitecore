﻿using System;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Sitecore.Test.Common
{
    public class AssetDbContextFactory
    {
        public static AssetDbContext Create()
        {
            var options = new DbContextOptionsBuilder<AssetDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var context = new AssetDbContext(options);

            context.Database.EnsureCreated();

            return context;
        }

        public static void Destroy(AssetDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }
    }
}
