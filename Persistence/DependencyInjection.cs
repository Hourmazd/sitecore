﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Application.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace Persistence
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AssetDbContext>(opt =>
            {
                opt.UseLazyLoadingProxies();
                opt.UseSqlite(configuration.GetConnectionString("DefaultConnection"));
            });
            
            services.AddScoped<IAssetDbContext>(provider => provider.GetService<AssetDbContext>());
            
            return services;
        }
    }
}
