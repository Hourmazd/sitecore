﻿using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;
using Domain.Entities;
using Application.Interfaces;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Persistence
{
    public class AssetDbContext : IdentityDbContext, IAssetDbContext
    {
        public AssetDbContext([NotNullAttribute] DbContextOptions<AssetDbContext> options)
            : base(options)
        {
        }

        public DbSet<Asset> Assets { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Asset>(entity =>
            {
                entity.Property((e => e.AssetId))
                    .ValueGeneratedOnAdd()
                    .HasDefaultValueSql("newid()");
            });
        }
    }
}
