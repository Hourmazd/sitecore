﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace WebExperience.Test.Pages.Account
{
    public class RegisterUserModel : PageModel
    {
        private readonly IHttpClientFactory _ClientFactory;

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required] [EmailAddress] public string Email { get; set; }

            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Confirm Password")]
            [Compare("Password", ErrorMessage = "Confirmation password is not matched.")]
            public string ConfirmPassword { get; set; }
        }

        public RegisterUserModel(IHttpClientFactory clientFactory)
        {
            _ClientFactory = clientFactory;
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                var client = _ClientFactory.CreateClient();
                var req = new HttpRequestMessage(HttpMethod.Post, "http://localhost:8080/api/account/register/" + Input.Email + "/" + Input.Password);
                var response = await client.SendAsync(req);

                if (response.IsSuccessStatusCode)
                {
                    var responseStream = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<IdentityResult>(responseStream);

                    if (!result.Errors.Any())
                    {
                        var user = new IdentityUser(Input.Email);
                        var userContent = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
                        var signInResult = await client.PostAsync("http://localhost:8080/api/account/signin", userContent);

                        if (signInResult.IsSuccessStatusCode)
                        {
                            return RedirectToPage("/Asset/AssetList");
                        }
                        else
                        {
                            foreach (var error in result.Errors)
                            {
                                ModelState.AddModelError("", error.Description);
                            }
                        }
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError("", error.Description);
                        }
                    }
                }
            }

            return null;
        }
    }
}