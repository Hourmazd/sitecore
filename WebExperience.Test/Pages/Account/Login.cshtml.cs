﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace WebExperience.Test.Pages.Account
{
    public class LoginModel : PageModel
    {
        private readonly IHttpClientFactory _ClientFactory;

        [BindProperty] 
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required] [EmailAddress] public string Email { get; set; }

            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Required] public bool RememberMe { get; set; }
        }

        public LoginModel(IHttpClientFactory clientFactory)
        {
            _ClientFactory = clientFactory;
        }

        public void OnGet()
        {
        }

        public IActionResult OnPostRedirectToRegister()
        {
            return RedirectToPage("RegisterUser");
        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                var client = _ClientFactory.CreateClient();
                var req = new HttpRequestMessage(HttpMethod.Post, "http://localhost:8080/api/account/signin/" + Input.Email + "/" + Input.Password + "/" + Input.RememberMe);
                var response = await client.SendAsync(req);

                var responseStream = await response.Content.ReadAsStringAsync();
                var result = JsonSerializer.Deserialize<bool>(responseStream);

                if (result)
                {
                    var user = new IdentityUser(Input.Email);
                    var userContent = new StringContent(JsonSerializer.Serialize(user), Encoding.UTF8, "application/json");
                    var signInResult = await client.PostAsync("http://localhost:8080/api/account/signin", userContent);

                    if (signInResult.IsSuccessStatusCode)
                    {
                        return RedirectToPage("/Asset/AssetList");
                    }
                    else
                    {
                        ModelState.AddModelError("", signInResult.ToString());
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Unable to login. Please check username and password.");
                }
            }

            return null;
        }
    }
}