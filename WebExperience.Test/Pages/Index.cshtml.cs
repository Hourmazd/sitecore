﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using JW;
using Microsoft.AspNetCore.Http;
using Domain.Entities;
using Newtonsoft.Json;

namespace WebExperience.Test.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IHttpClientFactory _ClientFactory;

        public IndexModel(IHttpClientFactory clientFactory)
        {
            _ClientFactory = clientFactory;
        }

        public IActionResult OnGet(int p = 1)
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToPage("/Asset/AssetList");
            else
                return RedirectToPage("/Account/Login");
        }
    }
}
