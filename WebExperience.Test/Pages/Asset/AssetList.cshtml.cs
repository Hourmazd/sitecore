﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Domain.Entities;
using JW;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace WebExperience.Test.Pages.Asset
{
    public class AssetListModel : PageModel
    {
        private readonly IHttpClientFactory _ClientFactory;
        public IEnumerable<Domain.Entities.Asset> Items { get; set; }
        public Pager Pager { get; set; }
        public int TotalItems { get; set; }
        public int PageSize { get; set; }
        public int MaxPages { get; set; }

        public AssetListModel(IHttpClientFactory clientFactory)
        {
            _ClientFactory = clientFactory;
        }

        public async Task OnGet(int p = 1)
        {
            TotalItems = 100000;
            PageSize = 10;
            MaxPages = 10;

            Pager = new Pager(TotalItems, p, PageSize, MaxPages);

            var client = _ClientFactory.CreateClient();
            var req = new HttpRequestMessage(HttpMethod.Get, "http://localhost:8080/api/asset/" + Pager.PageSize + "/" + Pager.CurrentPage);
            var response = await client.SendAsync(req);
            
            if (response.IsSuccessStatusCode)
            {
                var responseStream = await response.Content.ReadAsStringAsync();
                Items = JsonConvert.DeserializeObject<List<Domain.Entities.Asset>>(responseStream);
            }
            else
            {
                ModelState.AddModelError("", response.ReasonPhrase);
            }
        }

        public IActionResult OnPost(int totalItems, int pageSize, int maxPages)
        {
            // update pager parameters for session and redirect back to 'OnGet'
            HttpContext.Session.SetInt32("TotalItems", totalItems);
            HttpContext.Session.SetInt32("PageSize", pageSize);
            HttpContext.Session.SetInt32("MaxPages", maxPages);
            return Redirect("/");
        }
    }
}