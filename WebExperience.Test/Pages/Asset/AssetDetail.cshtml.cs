﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Domain.Entities;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebExperience.Test.Pages.Asset
{
    public class AssetDetailModel : PageModel
    {
        private readonly IHttpClientFactory _ClientFactory;
        public Domain.Entities.Asset Item { get; set; }

        public AssetDetailModel(IHttpClientFactory clientFactory)
        {
            _ClientFactory = clientFactory;
        }

        public async Task OnGet(Guid id)
        {
            var client = _ClientFactory.CreateClient();
            var req = new HttpRequestMessage(HttpMethod.Get, "http://localhost:8080/api/asset/" + id);
            var response = await client.SendAsync(req);

            if (response.IsSuccessStatusCode)
            {
                var responseStream = await response.Content.ReadAsStringAsync();
                Item = JsonConvert.DeserializeObject<Domain.Entities.Asset>(responseStream);
            }
            else
            {
                ModelState.AddModelError("", response.ReasonPhrase);
            }
        }
    }
}