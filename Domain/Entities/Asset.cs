﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Asset
    {
        [Key]
        [CsvHelper.Configuration.Attributes.Name("asset id")]
        public Guid AssetId { get; set; }

        [CsvHelper.Configuration.Attributes.Name("file_name")]
        public string FileName { get; set; }

        [CsvHelper.Configuration.Attributes.Name("mime_type")]
        public string MimeType { get; set; }

        [CsvHelper.Configuration.Attributes.Name("created_by")]
        public string CreatedBy { get; set; }

        [CsvHelper.Configuration.Attributes.Name("email")]
        public string Email { get; set; }

        [CsvHelper.Configuration.Attributes.Name("country")]
        public string Country { get; set; }

        [CsvHelper.Configuration.Attributes.Name("description")]
        public string Description { get; set; }
    }
}
