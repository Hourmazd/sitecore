﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic data retrieval from JSON test
    /// </summary>
    public class JsonReadingTest : ITest
    {
        public string Name => "JSON Reading Test";

        public void Run()
        {
            var jsonData = Resources.SamplePoints;

            // TODO: 
            // Determine for each parameter stored in the variable below, the average value, lowest and highest number.
            // Example output
            // parameter   LOW AVG MAX
            // temperature   x   y   z
            // pH            x   y   z
            // Chloride      x   y   z
            // Phosphate     x   y   z
            // Nitrate       x   y   z

            PrintOverview(jsonData);            
        }

        private void PrintOverview(byte[] data)
        {
            var jsonStr = Encoding.UTF8.GetString(data);
            var sampleData = JsonConvert.DeserializeObject<SampleData>(jsonStr);

            Console.WriteLine("parameter\tLOW\tAVG\tMAX");
            Console.WriteLine($"temperature\t{sampleData.Values.Min(e => e.Temperature).ToString("F1")}\t{sampleData.Values.Average(e => e.Temperature).ToString("F1")}\t{sampleData.Values.Max(e => e.Temperature).ToString("F1")}");
            Console.WriteLine($"pH\t\t{sampleData.Values.Min(e => e.Ph).ToString("F1")}\t{sampleData.Values.Average(e => e.Ph).ToString("F1")}\t{sampleData.Values.Max(e => e.Ph).ToString("F1")}");
            Console.WriteLine($"Chloride\t{sampleData.Values.Min(e => e.Chloride).ToString("F1")}\t{sampleData.Values.Average(e => e.Chloride).ToString("F1")}\t{sampleData.Values.Max(e => e.Chloride).ToString("F1")}");
            Console.WriteLine($"Phosphate\t{sampleData.Values.Min(e => e.Phosphate).ToString("F1")}\t{sampleData.Values.Average(e => e.Phosphate).ToString("F1")}\t{sampleData.Values.Max(e => e.Phosphate).ToString("F1")}");
            Console.WriteLine($"Nitrate\t\t{sampleData.Values.Min(e => e.Nitrate).ToString("F1")}\t{sampleData.Values.Average(e => e.Nitrate).ToString("F1")}\t{sampleData.Values.Max(e => e.Nitrate).ToString("F1")}");
        }
    }

    public class SampleData
    {
        [JsonProperty("samples")]
        public IList<Values> Values { get; set; }
    }

    public class Values
    {
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("temperature")]
        public decimal Temperature { get; set; }

        [JsonProperty("pH")]
        public decimal Ph { get; set; }

        [JsonProperty("phosphate")]
        public decimal Phosphate { get; set; }

        [JsonProperty("chloride")]
        public decimal Chloride { get; set; }

        [JsonProperty("nitrate")]
        public decimal Nitrate { get; set; }
    }
}
