﻿using System.IO;
using System.Text;
using System;
using System.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    public class CsvProcessingTest : ITest
    {
        public void Run()
        {
            // TODO
            // Create a domain model via POCO classes to store the data available in the CSV file below
            // Objects to be present in the domain model: Asset, Country and Mime type
            // Process the file in the most robust way possible
            // The use of 3rd party plugins is permitted

            var csvFile = Resources.AssetImport;
            var byteArray = Encoding.ASCII.GetBytes(csvFile);
            var stream = new MemoryStream(byteArray);

            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                using (var csvReader = new CsvHelper.CsvReader(reader))
                {
                    var result = csvReader.GetRecords<Asset>().ToArray();
                }
            }
        }
    }

    public class Asset
    {
        [CsvHelper.Configuration.Attributes.Name("asset id")]
        public Guid AssetId { get; set; }

        [CsvHelper.Configuration.Attributes.Name("mime_type")]
        public string MimeType { get; set; }

        [CsvHelper.Configuration.Attributes.Name("country")]
        public string Country { get; set; }
    }
}
