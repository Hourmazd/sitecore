﻿using ImageProcessor;
using System.IO;
using System.Drawing;
using System.Net;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Image rescaling
    /// </summary>
    public class RescaleImageTest : ITest
    {
        public void Run()
        {
            // TODO
            // Grab an image from a public URL and write a function that rescales the image to a desired format
            // The use of 3rd party plugins is permitted
            // For example: 100x80 (thumbnail) and 1200x1600 (preview)

            var thumbnailSize = new Size(100, 80);
            var previewSize = new Size(1200, 1600);

            using (var wc = new WebClient())
            {
                var bytes = wc.DownloadData("https://sitecorecdn.azureedge.net/-/media/sitecoresite/images/home/_/cta-overlapped-image/cta-overlapped-image-right-gettyimages-614205854-597x400.jpg");

                using (var inStream = new MemoryStream(bytes))
                {
                    using (var thumbnailStream = new MemoryStream())
                    {
                        using (var imageFactory = new ImageFactory())
                        {
                            imageFactory.Load(inStream)
                                        .Resize(thumbnailSize)
                                        .Save(thumbnailStream);
                        }
                    }

                    using (var previewStream = new MemoryStream())
                    {
                        using (var imageFactory = new ImageFactory())
                        {
                            imageFactory.Load(inStream)
                                        .Resize(previewSize)
                                        .Save(previewStream);
                        }
                    }
                }
            }
        }
    }
}
