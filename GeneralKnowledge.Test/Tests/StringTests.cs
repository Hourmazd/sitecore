﻿using System;
using System.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic string manipulation exercises
    /// </summary>
    public class StringTests : ITest
    {
        public void Run()
        {
            // TODO
            // Complete the methods below

            AnagramTest();
            GetUniqueCharsAndCount();
        }

        private void AnagramTest()
        {
            var word = "stop";
            var possibleAnagrams = new string[] { "test", "tops", "spin", "post", "mist", "step" };

            foreach (var possibleAnagram in possibleAnagrams)
            {
                Console.WriteLine($"{word} > {possibleAnagram}: {possibleAnagram.IsAnagram(word)}");
            }
        }

        private void GetUniqueCharsAndCount()
        {
            const string word = "xxzwxzyzzyxwxzyxyzyxzyxzyzyxzzz";

            // TODO
            // Write an algorithm that gets the unique characters of the word below 
            // and counts the number of occurrences for each character found

            var result = word.GroupBy(e => e).ToDictionary(e => e.Key, e => e.Count());

            foreach (var pair in result)
            {
                Console.WriteLine($"{pair.Key} > {pair.Value}");
            }
        }
    }

    public static class StringExtensions
    {
        public static bool IsAnagram(this string a, string b)
        {
            // TODO
            // Write logic to determine whether a is an anagram of b

            var aParsed = a.GroupBy(e => e).ToDictionary(e => e.Key, e => e.Count());
            var bParsed = b.GroupBy(e => e).ToDictionary(e => e.Key, e => e.Count());

            return aParsed.All(pair => bParsed.ContainsKey(pair.Key) && bParsed[pair.Key] == pair.Value);
        }
    }
}
