## Project Spesifications
Arcitecture: Clean Arcitecture (Monolith)
Platform: Cross Platform

## Technologies
* .NET Core 3
* ASP.NET Core 3
* Entity Framework Core 3
* Entity Core Identity

##Ptterns
CQRS

## Tools
* MediatR
* Linq

## Prerequisites
You will need the following tools:

* [Visual Studio Code or Visual Studio 2019](https://visualstudio.microsoft.com/vs/)
* [.NET Core SDK 3](https://dotnet.microsoft.com/download/dotnet-core/3.0)

## Run
Follow these steps to get your development environment set up:

  1. Clone the repository
  2. In the Power Shell do the next steps for runing the server
  3. At the root directory, restore required packages by running:
     dotnet restore
     
  4. Next, build the solution by running:
     dotnet build
     
  5. Next, within the `\WebUI.API` directory, launch the back end by:
     dotnet [watch] run
     
  6. Server is runing and lisening on port 8080. Access to api endpoints by http://localhost:8080/api
		* Login by http://localhost:8080/api/account/login}
		* Get the assets by http://localhost:8080/api/asset/{pageSize}/{page}
		* Get one asset by http://localhost:8080/api/asset/{id}

  7. Run the WebExperience.Test project in IIS[Express] to access to the WebApplicaton
	 
## Some Descriptions
* Since we have a filled csv file for this exersice, it is not needed to develop Migration Scripts and filling the db, but I did it becuase I wanted to develop Clean(Monolith) Arcitecture from the begining
